<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_quotes', function (Blueprint $table) {
            $table->integer('message_id');
            $table->integer('quote_id');

            $table->foreign('message_id')->references('id')->on('message')->onDelete('cascade');
            $table->foreign('quote_id')->references('id')->on('message')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_quotes');
    }
}
