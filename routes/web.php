<?php

$router->group(['prefix' => 'api/v1/'], function() use ($router){

    $router->post('user/login', 'UserController@login');

    $router->group(['middleware' => 'auth'], function () use ($router){

        $router->get('chat/{id}', 'ChatController@show');
        $router->post('chat', 'ChatController@store');
        $router->delete('chat/{chat}', 'ChatController@delete');

        $router->get('subscribe', 'SubscribeController@index');
        $router->post('subscribe', 'SubscribeController@store');
        $router->delete('subscribe', 'SubscribeController@deleteAll');
        $router->delete('subscribe/{chat}', 'SubscribeController@delete');

        $router->get('chat/{chat}/message', 'MessageController@show');
        $router->post('chat/{chat}/message', 'MessageController@store');

    });

});
