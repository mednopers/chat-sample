<?php

namespace App\Http\Controllers;

use App\Events\ChatMessage;
use App\Http\Requests\Message\CreateRequest;
use App\Models\Chat;
use App\Models\Message;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller;

class MessageController extends Controller
{
    public function show(Chat $chat)
    {
        $messages = $chat->messages()->orderByDesc('created_at')
                        ->with(['user', 'quotes'])->paginate();
        return response()->json($messages, Response::HTTP_OK);
    }

    public function store(CreateRequest $request, Chat $chat)
    {
        $message = Message::create([
            'chat_id' => $chat->id, 
            'user_id' => Auth::id(), 
            'text' => e($request->input('text'))
        ]);
        
        if (count($request->input('quotes'))){
            $message->quotes()->attach($request->input('quotes'));
        }

        event(new ChatMessage($chat));

        return response()->json(null, Response::HTTP_CREATED);
    }
}