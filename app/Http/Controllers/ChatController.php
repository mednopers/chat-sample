<?php

namespace App\Http\Controllers;

use App\Http\Requests\Chat\CreateRequest;
use App\Models\Chat;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller;

class ChatController extends Controller
{
    public function show($id)
    {
        $chat = Chat::where(['id' => $id])->first();

        if (!$chat){
            return response()->json(null, Response::HTTP_NO_CONTENT);
        }

        return response()->json($chat, Response::HTTP_OK);
    }

    public function store(CreateRequest $request)
    {
        try{
            $chat = Chat::create([
                'title' => e($request->input('title')),
                'node_id' => $request->input('node_id'),
                'user_count' => 1,
                'service_code' => $this->service->getCode(),
                'blocked' => 0
            ]);

            $chat->users()->attach(Auth::user());
        }
        catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return response()->json($chat, Response::HTTP_CREATED);
    }

    public function delete(Chat $chat)
    {
        $chat->users()->detach();
        $chat->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}