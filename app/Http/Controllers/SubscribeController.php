<?php

namespace App\Http\Controllers;

use App\Http\Requests\Chat\SubscribeRequest;
use App\Models\Chat;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller;

class SubscribeController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function index()
    {
        if (!count($this->user->chats)){
            return response()->json([], Response::HTTP_NO_CONTENT);
        }
        return response()->json($this->user->chats, Response::HTTP_OK);
    }

    public function store(SubscribeRequest $request)
    {
        if (!$this->user->chats->contains($request->input('chat_id'))){
            $this->user->chats()->attach($request->input('chat_id'));
        }

        return response()->json(null, Response::HTTP_CREATED);
    }

    public function delete(Chat $chat)
    {
        $this->user->chats()->detach($chat->id);
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }

    public function deleteAll()
    {
        $this->user->chats()->detach();
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}