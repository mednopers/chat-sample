<?php

namespace App\Http\Requests\Message;

use Urameshibr\Requests\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'text'      =>  'required|filled|string',
            'quotes'    =>  'array'
        ];
    }
}