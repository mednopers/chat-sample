<?php

namespace App\Http\Requests\Chat;

use Urameshibr\Requests\FormRequest;

class SubscribeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chat_id'   =>  'required|exists:chat,id'
        ];
    }
}