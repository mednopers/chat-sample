<?php

namespace App\Http\Requests\Chat;

use Urameshibr\Requests\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'     =>  'required|filled|string|max:80',
            'node_id'   =>  'required|filled|integer|min:1',
        ];
    }
}