<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use phpcent\Client;
use phpcent\Transport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Client::class, function ($app) {
            $client = new Client(config('centrifugo.url'));
            $client->setSecret(config('centrifugo.secret'));
            Transport::setSafety(Transport::SAFE);
            return $client;
        });
    }
}
