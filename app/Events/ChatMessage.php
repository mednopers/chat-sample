<?php

namespace App\Events;

use App\Models\Chat;

class ChatMessage extends Event
{
    public $chat;

    public function __construct(Chat $chat)
    {
        $this->chat = $chat;
    }
}