<?php

namespace App\Listeners;

use App\Events\Event;
use phpcent\Client;

class SocketSender
{
    private $client;
    private $channel;

    public function __construct(Client $client, $channel)
    {
        $this->client = $client;
        $this->channel = $channel;
    }

    public function handle(Event $event)
    {
        $this->guardNotEmptyChannel();
        $usersLogin = [];
        foreach ($event->chat->users as $chatUser){
            $usersLogin[] = $chatUser->login;
        }

        $this->client->publish($this->channel, [
            'node_id' => $event->chat->node_id,
            'chat_id' => $event->chat->id,
            'users' => $usersLogin
        ]);
    }

    private function guardNotEmptyChannel()
    {
        if (empty($this->channel)){
            throw new \InvalidArgumentException('Channel can`t be blank.');
        }
    }
}