<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User2Chat extends Model
{
    public $timestamps = false;

    protected $table = 'user2chat';

    protected $fillable = [
        'chat_id', 'user_id'
    ];
}