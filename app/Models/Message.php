<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'message';

    protected $fillable = [
        'chat_id', 'user_id', 'text'
    ];

    protected $hidden = ['pivot'];

    public function chat()
    {
        return $this->hasOne(Chat::class);
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function quotes()
    {
        return $this->belongsToMany(Message::class, 'message_quotes', 'message_id', 'quote_id')
            ->with('user');
    }
}