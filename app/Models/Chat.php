<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chat';

    protected $fillable = [
        'title', 'node_id', 'user_count', 'blocked'
    ];

    protected $hidden = [
        'pivot'
    ];

    public function users(){
        return $this->belongsToMany(User::class, 'user2chat');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function scopeActive(Builder $query, int $nodeId = null)
    {
        if ($nodeId){
            $query->where('node_id', '=', $nodeId);
        }

        return $query->where('user_count', '>', 0)
            ->where('blocked', '=', 0)
            ->orderBy('user_count')
            ->orderByDesc('created_at');
    }
}